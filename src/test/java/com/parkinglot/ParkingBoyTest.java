package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ParkingBoyTest {
    @Test
    void should_return_a_ticket_when_parkingboy_park_the_car_given_a_car(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(Collections.singletonList(new ParkingLot(10)));
        Car car = new Car();

        // When
        ParkingTicket ticket = parkingBoy.park(car);

        // Then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_parkingboy_park_the_car_given_a_ticket(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(Arrays.asList(new ParkingLot(10)));
        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);
        // When
        Car fetchedCar = parkingBoy.fetch(ticket);

        // Then
        Assertions.assertSame(car,fetchedCar);
    }

    @Test
    void should_return_correct_two_cars_when_parkingboy_park_the_car_given_two_ticket_and_a_parking_lot(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(Collections.singletonList(new ParkingLot(10)));
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket ticket1 = parkingBoy.park(car1);
        ParkingTicket ticket2 = parkingBoy.park(car2);

        // When
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);


        // Then
        Assertions.assertSame(car1,fetchedCar1);
        Assertions.assertSame(car2,fetchedCar2);

    }

    @Test
    void should_return_unrecognized_parking_ticket_exception_when_parkingboy_park_the_car_given_wrong_tickets(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(Collections.singletonList(new ParkingLot(10)));
        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);
        ParkingTicket wrongTicket = new ParkingTicket();

        // When


        // Then
        Assertions.assertThrows(UnrecognizedParkingTicketException.class,()->parkingBoy.fetch(wrongTicket));

    }

    @Test
    void should_return_unrecognized_parking_ticket_exception_when_parkingboy_park_the_car_given_has_already_used_tickets(){
        // Given
        ParkingBoy parkingBoy = new ParkingBoy(Collections.singletonList(new ParkingLot(10)));
        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);

        // When
        Car fetchedCar = parkingBoy.fetch(ticket);

        // Then
        Assertions.assertThrows(UnrecognizedParkingTicketException.class,()->parkingBoy.fetch(ticket));

    }

    @Test
    void should_return_the_2th_parkinglot_ticket_when_boy_park_over_first_parkinglot_capacity_given_two_parkinglot(){
        // Given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        List<ParkingLot> parkingLots = Arrays.asList(parkingLot1, parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        parkingBoy.park(new Car());
        Car car = new Car();
        ParkingTicket parkingTicket = parkingBoy.park(car);
        // When
        Car fetchedCar = parkingLot2.fetch(parkingTicket);

        // Then
        Assertions.assertSame(car,fetchedCar);
    }

    @Test
    void should_return_no_available_exception_when_boy_park_over_two_parkinglot_capacity_given_two_parkinglot(){
        // Given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = Arrays.asList(parkingLot1, parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());

        // When

        // Then
        Assertions.assertThrows(NoAvailableException.class,() -> parkingBoy.park(new Car()));

    }
}
