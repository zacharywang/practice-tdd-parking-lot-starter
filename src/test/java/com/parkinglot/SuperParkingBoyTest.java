package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class SuperParkingBoyTest {
    @Test
    public void should_return_high_vacancy_rate_parking_lot_when_super_boy_parking_the_car_given_a_car(){
        // Given
        ParkingLot parkingLot1 = new ParkingLot(2);
        // 空置率 0.5
        parkingLot1.park(new Car());

        ParkingLot parkingLot2 = new ParkingLot(10);
        // 空置率 0.9
        parkingLot2.park(new Car());
        SuperBoy superBoy = new SuperBoy(Arrays.asList(parkingLot1,parkingLot2));
        // When
        Car car = new Car();
        ParkingTicket parkingTicket = superBoy.park(car);
        Car fetchedCar = parkingLot2.fetch(parkingTicket);

        // Then
        Assertions.assertSame(car,fetchedCar);

    }
}
