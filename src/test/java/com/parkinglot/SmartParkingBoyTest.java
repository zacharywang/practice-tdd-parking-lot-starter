package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class SmartParkingBoyTest {
    @Test
    public void should_return_the_more_position_parking_lot_when_smart_parking_boy_park_the_car_given_two_parking_lot_and_some_car() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);

        // When
        Car car = new Car();
        ParkingTicket parkingTicket = new SmartParkingBoy(Arrays.asList(parkingLot1, parkingLot2)).park(car);
        Car fetchedCar = parkingLot2.fetch(parkingTicket);

        // Then
        Assertions.assertSame(car,fetchedCar);
    }
}
