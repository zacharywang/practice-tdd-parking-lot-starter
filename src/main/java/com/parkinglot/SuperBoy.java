package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SuperBoy extends ParkingBoy {
    public SuperBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    @Override
    public ParkingTicket park(Car car) throws NoAvailableException {
        ParkingLot highestVacancyRateParkingLot;

        highestVacancyRateParkingLot = getParkingLots()
                .stream()
                .max(Comparator.comparingDouble(parkingLot -> (double) parkingLot.getAvailableCapacity() / parkingLot.getCapacity()))
                .orElse(null);

        if (highestVacancyRateParkingLot == null) {
            throw new NoAvailableException();
        } else {
            return highestVacancyRateParkingLot.park(car);
        }
    }
}