package com.parkinglot;

import java.util.List;

public class ParkingBoy {
    private List<ParkingLot> parkingLots;

    public List<ParkingLot> getParkingLots() {
        return parkingLots;
    }

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingTicket park(Car car) throws NoAvailableException {
        for (ParkingLot parkingLot : parkingLots) {
            if (!parkingLot.isFull()) {
                return parkingLot.park(car);
            }
        }

        throw new NoAvailableException();
    }

    public Car fetch(ParkingTicket ticket)  {
        for (ParkingLot parkingLot : parkingLots) {
            try {
                return parkingLot.fetch(ticket);
            } catch (UnrecognizedParkingTicketException ex) {

            }
        }

        throw new UnrecognizedParkingTicketException();
    }
}
