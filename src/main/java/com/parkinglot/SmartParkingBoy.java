package com.parkinglot;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SmartParkingBoy extends ParkingBoy {
    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    @Override
    public ParkingTicket park(Car car) throws NoAvailableException {
        List<ParkingLot> availableParkingLots =
                getParkingLots()
                        .stream()
                        .filter(parkingLot -> !parkingLot.isFull())
                        .sorted(new CapacityComparator())
                        .collect(Collectors.toList());

        if (availableParkingLots.isEmpty()) {
            throw new NoAvailableException();
        } else {
            return availableParkingLots.get(0).park(car);
        }
    }

    private static class CapacityComparator implements Comparator<ParkingLot> {
        @Override
        public int compare(ParkingLot o1, ParkingLot o2) {
            return Integer.compare(o2.getAvailableCapacity(), o1.getAvailableCapacity());
        }
    }
}
