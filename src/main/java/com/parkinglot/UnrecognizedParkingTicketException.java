package com.parkinglot;

public class UnrecognizedParkingTicketException extends RuntimeException{
    public UnrecognizedParkingTicketException(){}
    public UnrecognizedParkingTicketException(String message) {
        super("Unrecognized ParkingTicket Exception!");
    }
}
