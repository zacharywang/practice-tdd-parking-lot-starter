package com.parkinglot;

public class NoAvailableException extends RuntimeException{
    public NoAvailableException(){}
    public NoAvailableException(String message){
        super("No Available Exception !");
    }
}
