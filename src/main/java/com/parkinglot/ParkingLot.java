package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private Map<ParkingTicket, Car> ticketCarMap;
    private int capacity;
    private int currentSize;

    public ParkingLot(int capacity) {
        this.capacity = capacity;
        this.currentSize = 0;
        this.ticketCarMap = new HashMap<>();
    }

    public ParkingTicket park(Car car) {
        if (isFull()) {
            throw new NoAvailableException();
        }
        ParkingTicket ticket = new ParkingTicket();
        ticketCarMap.put(ticket, car);
        currentSize++;
        return ticket;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if (parkingTicket == null || !ticketCarMap.containsKey(parkingTicket)) {
            throw new UnrecognizedParkingTicketException();
        }

        Car car = ticketCarMap.remove(parkingTicket);
        currentSize--;

        if (car == null) {
            throw new UnrecognizedParkingTicketException();
        }

        return car;
    }

    public boolean isFull() {
        return currentSize >= capacity;
    }

    public int getAvailableCapacity() {
        return capacity - currentSize;
    }

    public int getCapacity() {
        return capacity;
    }
}
