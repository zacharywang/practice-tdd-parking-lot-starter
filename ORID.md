# ORID

## O

​	Today, we started the morning with a code review, from which we learned quite a bit. And when I was trying to use the imperative pattern to reduce a lot of if else statements in a function, I had problems using that design pattern because I didn't understand the imperative pattern well enough. Then there was an exercise for a parking-lot project. The first step is tasking, which is also difficult and requires a thorough understanding of the requirements. Then later when writing code, it is also important to build out the tests first and drive development based on the tests.

## R

substantiate

## I

​	This parking-lot project exercise is a good way to consolidate what I have learned earlier and to better enhance my understanding of OOP ideas.

## D

​	Today's application of the Command Pattern can be applied to communication and collaboration involving multiple objects. The Command Pattern decouples the sender and receiver of a request, thus simplifying the code structure. In addition, the command pattern can realize the "undo" and "redo" functions of the editor.